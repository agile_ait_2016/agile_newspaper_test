package Frames;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	private JLabel l0 = new JLabel("Hello world!");
	private JPanel p1=new JPanel();
	
	public MyFrame(String s){
		super(s);
		Container content=getContentPane();
		content.setLayout(new FlowLayout());
		Font f=new Font("TimesRoman", Font.BOLD,20);
		p1.setLayout(new GridLayout(5,2));
		l0.setFont(f);
		content.add(l0);
		setSize(210,230);
		setVisible(true);
	}
}